<?php

/**
 * @file
 * Constants used by the "Covert Fields" module.
 *
 * @author
 * Guy Paddock (gapopl@rit.edu)
 */

/**
 * The name of the module.
 *
 * @var string
 */
define('CF_MODULE_NAME', 'Covert Fields');

/**
 * The internal name for the widget provided by this module.
 *
 * @var string
 */
define('CF_WIDGET_NAME', 'covert_field_widget');

/**
 * The internal name for the output format provided by this module.
 *
 * @var string
 */
define('CF_FORMATTER_NAME', 'covert_field');

/**
 * Name of the field on the content type edit form that provides the user with the ability to specify what message
 * should be displayed when field validation fails.
 *
 * @var string
 */
define('CF_FIELD_VALIDATION_FAIL_MSG', 'validation_failure_message');

/**
 * Name of the field on the content type edit form that provides the user with the ability to specify the PHP code for
 * validating the value entered by the user.
 *
 * @var string
 */
define('CF_FIELD_VALIDATION_CODE', 'validation_code');

/**
 * Name of the field on the content type edit form that provides the user with the ability to specify the PHP code for
 * preparing the field value that is stored in the database.
 *
 * @var string
 */
define('CF_FIELD_STORAGE_CODE', 'storage_code');

/**
 * Name of the field on the content type edit form that provides the user with the ability to specify the PHP code for
 * preparing the field value that is displayed to the user.
 *
 * @var string
 */
define('CF_FIELD_DISPLAY_CODE', 'display_code');
