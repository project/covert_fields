<?php

/**
 * @file
 * Defines the Covert Field formatter for the Content Construction Kit (CCK).
 *
 * @author
 * Guy Paddock (gapopl@rit.edu)
 */

/**
 * Implementation of hook_theme().
 */
function covert_fields_theme() {
  return array(
    'covert_fields_formatter_covert_field'  => array(
      'arguments'  => array('element' => NULL),
    )
  );
}

/**
 * Implementation of hook_field_formatter_info().
 */
function covert_fields_field_formatter_info() {
  return array(
    CF_FORMATTER_NAME => array(
      'label'       => t('Covert field display output'),
      'field types' => array(
        'text',
        'nodereference',
      ),
    ),
  );
}

/**
 * Theme function for 'Covert field display output' field formatter.
 *
 * @param $element
 *   The FAPI form element that is being themed.
 */
function theme_covert_fields_formatter_covert_field(array $element) {
  /* Since we're invoked by a FAPI element_validate callback, we don't get the CCK field object passed to us
   * as context, so we'll have to request it from CCK.
   */
  $field = _covert_fields_get_field_for_element($element);

  if (!empty($field)) {
    $columnName    = _covert_fields_get_default_column($field);

    $displayValue  = _covert_fields_get_display_value(
        $field,
        $element['#item']['#delta'], $element['#item'][$columnName]);
  }
  else {
    // This shouldn't happen...
    $displayValue  = $element['#value'];
  }

  return $displayValue;
}
