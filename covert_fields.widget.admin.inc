<?php

/**
 * @file
 * Defines the forms & logic for setting-up Covert Field widgets through the Content Construction Kit (CCK)
 * administration pages.
 *
 * @author
 * Guy Paddock (gapopl@rit.edu)
 */

/**
 * Implementation of hook_widget_settings().
 *
 * Handle the parameters for a widget.
 *
 * @param $op
 *   The operation to be performed. Possible values:
 *     - "form": Display the widget settings form.
 *     - "validate": Check the widget settings form for errors.
 *     - "save": Declare which pieces of information to save back to the database.
 *
 * @param $widget
 *   The widget on which the operation is to be performed.
 *
 * @return
 *   This varies depending on the operation.
 *     - The "form" operation should return an array of form elements to add to the settings page.
 *     - The "validate" operation has no return value. Use form_set_error().
 *     - The "save" operation should return an array of names of form elements to be saved in the database.
 */
function covert_fields_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      return covert_fields_widget_settings_form($widget);

    case 'validate':
      covert_fields_widget_settings_validate($widget);
      break;

    case 'save':
      return covert_fields_widget_settings_save($widget);
  }
}

/**
 * Implementation of the 'form' operation of covert_fields_widget_settings().
 *
 * Display the widget settings form.
 *
 * @param $widget
 *   The widget on which the operation is to be performed.
 *
 * @return
 *   An array of form elements to add to the settings page.
 */
function covert_fields_widget_settings_form($widget) {
  $form = array();

  if ($widget['type'] == CF_WIDGET_NAME) {
    $form[CF_FIELD_VALIDATION_FAIL_MSG] = array(
      '#type'          => 'textarea',
      '#title'         => t('CCK Field Validation Failure Message Override'),
      '#weight'        => 10,
      '#default_value' =>
        isset($widget[CF_FIELD_VALIDATION_FAIL_MSG]) ? $widget[CF_FIELD_VALIDATION_FAIL_MSG] : '',

      '#required'      => FALSE,
      '#description'   =>
        t('If you provide a message here, it will replace the message that CCK normally displays to ' .
          'the user if this field fails validation (i.e. value is in the wrong format, refers to the wrong post, etc). ' .
          'The replacement patterns available for use in your message are: ') .
          '<ul>'.
          '<li><code>%error</code> - ' . t('The original validation error message from CCK.') . '</li>'.
          '<li><code>%entered_value</code> - ' . t('The value entered by the user.') . '</li>'.
          '</ul>',
    );

    $form[CF_FIELD_VALIDATION_CODE] = array(
      '#type'          => 'textarea',
      '#title'         => t('Code for Validation'),
      '#weight'        => 20,
      '#default_value' => isset($widget[CF_FIELD_VALIDATION_CODE]) ? $widget[CF_FIELD_VALIDATION_CODE] : '',
      '#required'      => FALSE,
      '#rows'          => 10,
      '#description'   =>
        t('PHP code that validates what the user has entered. The code should not include &lt;?php ?&gt; delimiters. ' .
          'The variables available to your code are: ') .
          '<ul>' .
// TODO: Find out how to get access to the node object, likely via loading the form from the form cache.
//        '<li><code>$node</code> - ' . t('The node being saved.') . '</li>' .
          '<li><code>$field</code> - ' . t('The CCK field object.') . '</li>' .
          '<li><code>$delta</code> - ' . t('The field multiple-value index (i.e. field delta).') . '</li>' .
          '<li><code>$element</code> - ' . t('The Drupal FAPI form element object.') . '</li>' .

          '<li><code>$element_name_for_error</code> - ' .
          t('The name of the element, in the format expected by <code>form_set_error()</code>, to make raising '.
            'validation errors easier.') .

          '<li><code>$entered_value</code> - ' . t('The value entered by the user.') . '</li>' .
          '</ul>' .
        t('To raise a validation error, use <code>form_set_error()</code>.'),
    );

    $form[CF_FIELD_STORAGE_CODE] = array(
      '#type'          => 'textarea',
      '#title'         => t('Code for Stored Value'),
      '#weight'        => 20,
      '#default_value' => isset($widget[CF_FIELD_STORAGE_CODE]) ? $widget[CF_FIELD_STORAGE_CODE] : '',
      '#required'      => FALSE,
      '#rows'          => 10,
      '#description'   =>
        t('PHP code that returns the value to be stored for this field. The code should not '.
          'include &lt;?php ?&gt; delimiters. The variables available to your code are: ') .
          '<ul>' .
// TODO: Find out how to get access to the node object, likely via loading the form from the form cache.
//        '<li><code>$node</code> - ' . t('The node being saved.') . '</li>' .
          '<li><code>$field</code> - ' . t('The CCK field object.') . '</li>' .
          '<li><code>$delta</code> - ' . t('The field multiple-value index (i.e. field delta).') . '</li>' .
          '<li><code>$entered_value</code> - ' . t('The value entered by the user.') . '</li>' .
          '</ul>' .
        t('If this code does not return a value, the value entered by the user will be the ' .
          'stored value.'),
    );

    $form[CF_FIELD_DISPLAY_CODE] = array(
      '#type'          => 'textarea',
      '#title'         => t('Code for Displayed Value'),
      '#weight'        => 30,
      '#default_value' => isset($widget[CF_FIELD_DISPLAY_CODE]) ? $widget[CF_FIELD_DISPLAY_CODE] : '',
      '#required'      => FALSE,
      '#rows'          => 10,
      '#description'   =>
        t('PHP code that returns the value to be displayed for this field. The code ' .
          'should not include &lt;?php ?&gt; delimiters. ' .
          'The variables available to your code are: ') .
          '<ul>' .
// TODO: Find out how to get access to the node object.
//        '<li><code>$node</code> - ' . t('The node being displayed.') . '</li>' .
          '<li><code>$field</code> - ' . t('The CCK field object.') . '</li>' .
          '<li><code>$delta</code> - ' . t('The field multiple-value index (i.e. field delta).') . '</li>' .
// TODO: Find a way to save the value the user originally entered.
//        '<li><code>$entered_value</code> - ' . t('The value entered by the user.') . '</li>' .
          '<li><code>$stored_value</code> - ' . t('The value of the field that was stored.') . '</li>' .
          '</ul>' .
        t('If this code does not return a value, the stored value will be used for display.') .
          '<p><strong>' . t('Note:') . ' </strong>' .
        t('If you want the value returned by this code to be displayed when the node or node ' .
          'teaser is viewed, you need to set the &quot;Display&quot; for this field to ' .
          '&quot;Covert field display output&quot;.') . '</p>'
        );

    if (!user_access('Use PHP input for field settings (dangerous - grant with care)')) {
      // Replace the form elements with placeholders, so the user can see any existing code, but can't
      // modify it.
      _covert_fields_convert_to_markup_placeholder(CF_FIELD_VALIDATION_CODE, $form);
      _covert_fields_convert_to_markup_placeholder(CF_FIELD_STORAGE_CODE, $form);
      _covert_fields_convert_to_markup_placeholder(CF_FIELD_DISPLAY_CODE, $form);
    }
  }
  else {
    // Should never happen.
    drupal_set_message(
      t('@module does not know how to handle widget type "@type".',
        array(
          '@module' => CF_MODULE_NAME,
          '@type'   => $widget['type']
    )));
  }

  return $form;
}

/**
 * Implementation of the 'validate' operation of covert_fields_widget_settings().
 *
 * Check the widget settings form for errors.
 *
 * @param $widget
 *   The widget on which the operation is to be performed.
 */
function covert_fields_widget_settings_validate($widget) {
  // Validate code for validation.
  _covert_fields_widget_check_field_code_syntax($widget, CF_FIELD_VALIDATION_CODE);

  // Validate code for storage.
  _covert_fields_widget_check_field_code_syntax($widget, CF_FIELD_STORAGE_CODE);

  // Validate code for display.
  _covert_fields_widget_check_field_code_syntax($widget, CF_FIELD_DISPLAY_CODE);
}

/**
 * Implementation of the 'save' operation of covert_fields_widget_settings().
 *
 * Declare which pieces of information to save back to the database.
 *
 * @param $widget
 *   The widget on which the operation is to be performed.
 *
 * @return
 *   An array of names of form elements to be saved in the database.
 */
function covert_fields_widget_settings_save($widget) {
  return array(CF_FIELD_VALIDATION_FAIL_MSG, CF_FIELD_VALIDATION_CODE, CF_FIELD_STORAGE_CODE, CF_FIELD_DISPLAY_CODE);
}

/**
 * Checks the sytax of the PHP code, if any, that has been provided in the specified field of the widget's admin page.
 *
 * @param $widget
 *    The widget on which the operation is to be performed.
 *
 * @param $fieldName
 *    The name of the field that contains PHP code that is to be syntax checked.
 */
function _covert_fields_widget_check_field_code_syntax($widget, $fieldName) {
  if (!empty($widget[$fieldName]) && !_covert_fields_check_php_syntax($widget[$fieldName])) {
    form_set_error(
      $fieldName,
      t('The PHP code you have entered contains one or more syntax errors. Please correct it.'));
  }
}

/**
 * Replaces a PHP code field in the node settings form with a "placeholder" field that simply displays any code that
 * was previously set for the field, but does not allow the user to edit the code. This is used for users who do
 * not have the permission to input PHP code in for field settings.
 *
 * After this function returns, the specified original field will have been removed and replaced with a
 * placeholder.
 *
 * @param $realFieldName
 *   The name of the PHP code form field element that is to be replaced with a placeholder.
 *
 * @param $form
 *   A reference to the form array. The contents of the form array will be updated appropriately during the
 *   call to this function.
 */
function _covert_fields_convert_to_markup_placeholder($realFieldName, &$form) {
  $realFieldElement = $form[$realFieldName];

  unset($form[$realFieldName]);

  if (!empty($realFieldElement['#default_value'])) {
    $codeMarkup  = '<pre>' . check_plain($realFieldElement['#default_value']) . '</pre>';
    $description = t('This PHP code was set by an administrator.');
  }
  else {
    $codeMarkup  = t('&lt;none&gt;');
    $description = t("You're not allowed to input PHP code.");
  }

  $form[$realFieldName . '_markup'] = array(
    '#type'        => 'item',
    '#title'       => $realFieldElement['#title'],
    '#weight'      => $realFieldElement['#weight'],
    '#value'       => $codeMarkup,
    '#description' => $description,
  );
}
