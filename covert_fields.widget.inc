<?php

/**
 * @file
 * Defines the Covert Field widget for the Content Construction Kit (CCK).
 *
 * @author
 * Guy Paddock (gapopl@rit.edu)
 */

/**
 * Implementation of hook_widget_info().
 *
 * Declare information about a widget.
 *
 * @return
 *   An array keyed by widget name. Each element of the array is an associative array with these keys and
 *   values:
 *     - "label": The human-readable label for the widget.
 *     - "field types": An array of field type names that can be edited using this widget.
 *     - "multiple values": Either
 *     - CONTENT_HANDLE_CORE (default value) or
 *     - CONTENT_HANDLE_MODULE.
 *
 *  By default, the content module handles multiple values. Change this value only if you intend to
 *  handle multiple values in your module code.
 *
 *  - "callbacks": An array of callbacks. To set the default value, set the value of "default value" to
 *     one of the following values:
 *  - CONTENT_CALLBACK_DEFAULT (default value),
 *  - CONTENT_CALLBACK_CUSTOM, or
 *  - CONTENT_CALLBACK_NONE
 *
 * Multiple values and callbacks can be omitted if default handing is used. They're included in the code example
 * below just so this module can be used as an example for custom modules that might do things differently.
 */
function covert_fields_widget_info() {
  return array(
    CF_WIDGET_NAME => array(
      'label'       => 'Covert field',
      'field types' => array(
        'text',
        'nodereference',
        'userreference'
      ),
    ),
  );
}

/**
 * Implementation of hook_widget().
 *
 * Attach a single form element to the form. It will be built out and validated in the callback(s) listed in
 * hook_elements. We build it out in the callbacks rather than here in hook_widget so it can be plugged into any module
 * that can provide it with valid $field information.
 *
 * The content module will set the weight, field name, and delta values for each form element. This is a change from
 * earlier CCK versions where the widget managed its own multiple values.
 *
 * If there are multiple values for this field, the content module will call this function as many times as needed.
 *
 * @param $form
 *   The entire form array, $form['#node'] holds node information
 *
 * @param $form_state
 *   The form_state; $form_state['values'][$field['field_name']] holds the field's form values.
 *
 * @param $field
 *   The field array.
 *
 * @param $items
 *   Array of default values for this field.
 *
 * @param $delta
 *   The order of this item in the array of subelements (0, 1, 2, etc).
 *
 * @return
 *   The form item for a single element for this field.
 */
function covert_fields_widget(array &$form, array &$form_state, array $field, array $items, $delta = 0) {
  $element = array();

  if ($field['widget']['type'] == CF_WIDGET_NAME) {
    $columnName = _covert_fields_get_default_column($field);

    if (isset($items[$delta])) {
      $defaultValue =
        _covert_fields_get_display_value(
            $field,
            $delta,
            $items[$delta][$columnName]);
    }
    else {
      $defaultValue = '';
    }

    $element = array(
      '#type'             => 'text_textfield',
      '#element_validate' => array('covert_fields_widget_value_validate'),
      '#default_value'    => array($columnName => $defaultValue)
    );
  }
  else {
    // Should never happen.
    drupal_set_message(
        t('@module does not know how to handle widget type "@type".',
          array(
            '@module'  => CF_MODULE_NAME,
            '@type'    => $field['widget']['type']
          )));
  }

  return $element;
}

/**
 * Element validation callback for each value of a field using the Covert Field widget.
 *
 * For multiple-value fields, this function is invoked once for each value of the field.
 *
 * @param $element
 *   The FAPI form element representing the value being validated.
 *
 * @param $form_state
 *   The form state.
 */
function covert_fields_widget_value_validate(array $element, array &$form_state) {
  global $_covert_fields_error_cleanup_start_index;

  /* Since we're invoked by a FAPI element_validate callback, we don't get the CCK field object passed to us
   * as context, so we'll have to request it from CCK.
   */
  $field = _covert_fields_get_field_for_element($element);

  if (!empty($field)) {
    _covert_fields_invoke_custom_validation($element, $field, $form_state);

    /* We convert the values into storage format during the validation stage because it's the only
     * time we get access to the data before it gets passed to the underlying field.
     */
    _covert_fields_convert_to_storage_value($element, $field, $form_state);
  }
}

/**
 * Implementation of hook_nodeapi().
 *
 * This module implements this hook in order to fix-up any validation errors on fields using the Covert Fields
 * widget.
 *
 * @param $node
 *   The node the action is being performed on.
 *
 * @param $op
 *   What kind of action is being performed.
 *   This particular implementation only cares about "validate".
 *
 * @param $form
 *   When $op is "validate", this is the form array for the node edit form that is being validated.
 *
 * @param $unused
 *   This parameter is not used by this particular implementation.
 */
function covert_fields_nodeapi(&$node, $op, $form = NULL, $unused = NULL) {
  if ($op == 'validate') {
    $errors = form_get_errors();

    if (count($errors) > 0) {
      _covert_fields_filter_validation_errors($errors, $form);
    }
  }
}

/**
 * Return whether the specified string of PHP code is syntactically valid or not without executing it.
 *
 * Credit for this clever trick goes to luke@cywh.com on 2008-09-17 (http://php.net/manual/en/function.eval.php).
 *
 * @param $phpCode
 *   The string of PHP code for which a syntax check is desired.
 *
 * @return
 *   <code>true</code> if the code is syntactically valid, <code>false</code> if it is not.
 */
function _covert_fields_check_php_syntax($phpCode) {
  return @eval('return true; ' . $phpCode);
}

/**
 * Validates the value entered for the specified field using any custom PHP code for validation that was supplied by an
 * administrator.
 *
 * For multiple-value fields, this function is invoked once for each value of the field.
 *
 * @param $element
 *   The FAPI form element representing the field.
 *
 * @param $field
 *   The CCK field for which values are being converted.
 */
function _covert_fields_invoke_custom_validation(array $element, array $field) {
  global $_covert_fields_error_cleanup_start_index;

  // Prefix local variable names with an underscore to reduce the likelihood of being bashed.
  $_columnName    = _covert_fields_get_default_column($field);

  $delta                  = $element['#delta'];
  $entered_value          = $element['#value'][$_columnName];
  $element_name_for_error = $element['_error_element']['#value'];

  if (!empty($entered_value) && !empty($field['widget'][CF_FIELD_VALIDATION_CODE])) {
    eval($field['widget'][CF_FIELD_VALIDATION_CODE]);
  }

  $errors     = form_get_errors();
  $errorCount = count($errors);

  $_covert_fields_error_cleanup_start_index = $errorCount;

  return ($errorCount == 0);
}

/**
 * Convert the value entered for the specified field into the format for storage, using any custom PHP code for
 * storage that was supplied by an administrator.
 *
 * For multiple-value fields, this function is invoked once for each value of the field.
 *
 * @param $element
 *   The FAPI form element representing the field.
 *
 * @param $field
 *   The CCK field for which values are being converted.
 *
 * @param $form_state
 *   The form state.
 */
function _covert_fields_convert_to_storage_value(array $element, array $field, array &$form_state) {
  $delta      = $element['#delta'];
  $fieldName  = $field['field_name'];
  $columnName = _covert_fields_get_default_column($field);

  $storeValue = _covert_fields_get_storage_value($field, $delta, $element['#value'][$columnName]);

  /* Normally, the following statement could be just the following:
   *    $storeElementValue  = $element['#value'];
   *
   * However, this doesn't preserve the _error_element field that CCK adds to the form element value that is needed
   * during validation.
   */
  $storeElementValue = $form_state['values'][$fieldName][$delta];

  // We only want to modify the column value; the other array elements should be copied over as-is, to preserve
  // CCK metadata.
  $storeElementValue[$columnName] = $storeValue;

  form_set_value($element, $storeElementValue, $form_state);
}

/**
 * Invoke the PHP code for preparing the stored value, and returns the result. If the code doesn't return a
 * value, the value that was originally entered into the field is returned instead.
 *
 * @param $field
 *   The field array.
 *
 * @param $delta
 *   The field multiple-value index (i.e. field delta).
 *
 * @param $entered_value
 *   The value that the user entered into the field.
 *
 * @return
 *   The value to store for the field.
 */
function _covert_fields_get_storage_value(array $field, $delta, $entered_value) {
  // Prefix local variable names with an underscore to reduce the likelihood of being bashed.
  $_storedValue = $entered_value;

  if (!empty($entered_value) && !empty($field['widget'][CF_FIELD_STORAGE_CODE])) {
    $_returnedValue = eval($field['widget'][CF_FIELD_STORAGE_CODE]);

    if ($_returnedValue !== NULL) {
      $_storedValue = $_returnedValue;
    }
  }

  return $_storedValue;
}

/**
 * Invoke the PHP code for preparing the displayed value, and returns the result. If the code doesn't return a
 * value, the field's stored value is returned instead.
 *
 * @param $field
 *   The field array.
 *
 * @param $delta
 *   The field multiple-value index (i.e. field delta).
 *
 * @param $stored_value
 *   The stored value of the field.
 *
 * @return
 *   The value to display in the field.
 */
function _covert_fields_get_display_value(array $field, $delta, $stored_value) {
  // Prefix local variable names with an underscore to reduce the likelihood of being bashed.
  $_displayValue = $stored_value;

  if (!empty($stored_value) && !empty($field['widget'][CF_FIELD_DISPLAY_CODE])) {
    $_returnedValue = eval($field['widget'][CF_FIELD_DISPLAY_CODE]);

    if ($_returnedValue !== NULL) {
      $_displayValue = $_returnedValue;
    }
  }

  return $_displayValue;
}

/**
 * Filter the provided set of validation errors for the specified node edit form, fixing-up any errors that
 * pertain to fields that use the Covert Fields widget.
 *
 * @param $errors
 *   The array of validation errors, in the format returned by <code>form_get_errors()</code>.
 *
 * @param $form
 *   The form array for the node edit form that is raising validation errors.
 */
function _covert_fields_filter_validation_errors(array $errors, array $form) {
  global $_covert_fields_error_cleanup_start_index;

  /* Clear all form errors for now; they'll be re-populated in a bit.
   *
   * NOTE: This does NOT clear Drupal's messages, just its mapping of error messages to form elements.
   *       We selectively clear Drupal's messages later.
   */
  form_set_error(NULL, NULL, TRUE);

  $currentErrorIndex = 0;

  foreach ($errors as $key => $message) {
    /* Remove the current form-related error message from Drupal's messages, even if it doesn't pertain to our fields,
     * because we'll replace it later.
     */
    _covert_fields_remove_message('error', $message);

    // Don't clean-up error messages generated in the custom validation PHP code
    if ($errorIndex >= $_covert_fields_error_cleanup_start_index) {
      $message = _covert_fields_filter_validation_error($key, $message, $form);
    }

    // Put the error message back, for display to the user
    form_set_error($key, $message);

    ++$errorIndex;
  }
}

/**
 * Filter the provided validation error for the specified node edit form, fixing-up any error that pertains to a field
 * that uses the Covert Fields widget.
 *
 * @param $errorKey
 *   The key from Drupal's form error array; this is usually the path to the offending form element.
 *
 * @param $message
 *   The validation error message text.
 *
 * @param $form
 *   The form array for the node edit form that is raising validation errors.
 *
 * @return
 *   The filtered validation error message. If no filtering was required, this will be the same as the original message
 *   passed in for <code>$message</code>.
 */
function _covert_fields_filter_validation_error($errorKey, $message, array $form) {
  $element = _covert_fields_get_element_for_error($errorKey, $form);

  if (!empty($element)) {
    $field  = _covert_fields_get_field_for_element($element);
    $widget = $field['widget'];

    /* We only care about errors related to fields using our widget, and only when the widget
     * has custom error text.
     */
    if (!empty($widget) && ($widget['type'] == CF_WIDGET_NAME) && !empty($widget[CF_FIELD_VALIDATION_FAIL_MSG])) {
      $message = strtr(
        t($widget[CF_FIELD_VALIDATION_FAIL_MSG]),
        array(
         '%error'         => $message,
         '%entered_value' => trim($element['#value'])
        ));
    }
  }

  return $message;
}

/**
 * Utility function for looking-up the FAPI form element that corresponds to the specified validation error
 * key. The "error element key" is in the same format as the keys of arrays returned by
 * <code>form_get_errors()</code>.
 *
 * @param $errorElementKey
 *   The key of the desired element, in the key format returned by <code>form_get_errors()</code>.
 *   For example, "my_field][2][nid".
 *
 * @param $form
 *   The form array for the form that is raising validation errors.
 *
 * @return
 *   The FAPI form element that pertains to the specified error key, or <code>NULL</code> if no such
 *   element can be found.
 */
function _covert_fields_get_element_for_error($errorElementKey, array $form) {
  $element = NULL;

  if (!empty($errorElementKey) && !empty($form)) {
    // Kind of a hack, but faster than exploding, shifting, and then recursing
    $element = eval("return \$form[{$errorElementKey}];");
  }

  return $element;
}

/**
 * Utility function for removing the specified message from Drupal's set of current error messages.
 *
 * @param $type
 *   The type of message being removed.
 *
 * @param $message
 *   The message text itself.
 */
function _covert_fields_remove_message($type, $message) {
  $allMessages = drupal_get_messages($type);

  foreach ($allMessages[$type] as $currentMessage) {
    if ($currentMessage != $message) {
      drupal_set_message($currentMessage, $type);
    }
  }
}
